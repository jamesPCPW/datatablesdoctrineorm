<?php
namespace Brown298\DataTablesDoctrineORM\Test\DataTable;

use Brown298\DataTablesDoctrineORM\Model\DataTable\AbstractQueryBuilderDataTable;
use Brown298\DataTablesDoctrineORM\Model\DataTable\QueryBuilderDataTableInterface;

/**
 * Class QueryBuilderDataTable
 *
 * @package Brown298\DataTablesDoctrineORM\Test\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
class QueryBuilderDataTable extends AbstractQueryBuilderDataTable implements QueryBuilderDataTableInterface
{

}
