<?php
namespace Brown298\DataTablesDoctrineORM\Model\DataTable;

use Psr\Http\Message\RequestInterface;
use Brown298\DataTablesModels\Exceptions\ProcessorException;
use Brown298\DataTablesDoctrineORM\Service\Interfaces\RepositoryProcessInterface;
use Brown298\DataTablesModels\Service\ServerProcessService;
use Doctrine\ORM\EntityRepository;

/**
 * Class AbstractRepositoryDataTable
 *
 * @package Brown298\DataTablesDoctrineORM\Model\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
class AbstractRepositoryDataTable extends AbstractQueryBuilderDataTable implements RepositoryDataTableInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;

    /**
     * findAll
     *
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function findAll()
    {
        if (!($this->serverProcessService instanceof ServerProcessService)) {
           throw new ProcessorException('The container must be set');
        }

        if (!($this->repository instanceOf EntityRepository)) {
            throw new ProcessorException('The entity repository must be set');
        }

        $this->serverProcessService->setRepository($this->repository);

        return $this->serverProcessService->findAll();
    }

    /**
     * @param RequestInterface $request
     * @param null $dataFormatter
     *
     * @return JsonResponse|mixed|null
     */
    public function getData(RequestInterface $request, $dataFormatter = null)
    {
        if ($this->repository === null) {
            return null;
        }

        if ($this->serverProcessService->getRequest() === null) {
            $this->serverProcessService->setRequest($request);
        }

        $this->serverProcessService->setRepository($this->repository);

        return $this->execute($this->serverProcessService, $dataFormatter);
    }

    /**
     * findBy
     *
     * @param array $criteria
     * @param array $orderBy
     * @param null  $limit
     * @param null  $offset
     *
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        if (!($this->serverProcessService instanceof RepositoryProcessInterface)) {
            throw new ProcessorException('The process service must be set');
        }

        if (!($this->repository instanceOf EntityRepository)) {
            throw new ProcessorException('The entity repository must be set');
        }

        $this->serverProcessService->setRepository($this->repository);

        return $this->serverProcessService->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * __call
     *
     * @param $method
     * @param $arguments
     *
     * @return mixed
     * @throws \Brown298\DataTablesModels\Exceptions\ProcessorException
     */
    public function __call($method, $arguments)
    {
        if (!($this->serverProcessService instanceof RepositoryProcessInterface)) {
            throw new ProcessorException('The process service must be set');
        }

        if (!($this->repository instanceOf EntityRepository)) {
            throw new ProcessorException('The entity repository must be set');
        }

        $this->serverProcessService->setRepository($this->repository);

        return call_user_func_array(array($this->serverProcessService,$method), $arguments);
    }

    /**
     * setRepository
     * @param EntityRepository $repository
     * @return mixed|void
     */
    public function setRepository(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}
