<?php
namespace Brown298\DataTablesDoctrineORM\Model\DataTable;

/**
 * Interface RepositoryDataTableInterface
 *
 * @package Brown298\DataTablesDoctrineORM\Model\DataTable
 * @author  John Brown <brown.john@gmail.com>
 */
interface RepositoryDataTableInterface extends QueryBuilderDataTableInterface
{

}
