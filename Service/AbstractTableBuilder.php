<?php
namespace Brown298\DataTablesDoctrineORM\Service;

use \Brown298\DataTablesModels\Service\Interfaces\ServerProcessServiceInterface;

/**
 * Class AbstractTableBuilder
 *
 * @package Brown298\DataTablesDoctrineORM\Service
 */
abstract class AbstractTableBuilder implements TableBuilderInterface
{

    /**
     * @var \Brown298\DataTablesModelsBundle\Service\Interfaces\ServerProcessServiceInterface
     */
    protected $serverProcessService;

    /**
     * @var array
     */
    protected $args;

    /**
     * @var mixed
     */
    protected $table;

    /**
     * @var mixed
     */
    protected $tableConfig;

    /**
     * @var string
     */
    protected $tableId;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repository;


    /**
     * build
     *
     * @param array $args
     * @return mixed
     */
    public function build(array $args = array())
    {
        $this->args = $args;
        $this->createTable();
        $this->buildMetaData();
        $this->buildTable();

        return $this->table;
    }

    /**
     * @param $table
     * @return string
     */
    protected function getClassName($table)
    {
        if (isset($table->class) && $table->class !== null) {
            $className = $table->class;
        } else {
            $className = 'Brown298\DataTablesDoctrineORM\Test\DataTable\QueryBuilderDataTable';
        }
        return $className;
    }

    /**
     * parse
     *
     * implement parsing
     */
    abstract protected function parse();

    /**
     * creates a table
     *
     * @return mixed|object
     */
    protected function createTable()
    {
        $this->parse();
        $className = $this->getClassName($this->tableConfig);

        array_shift($this->args);

        if(is_array($this->args) && !empty($this->args) ) {
            $ref         = new \ReflectionClass($className);
            $this->table = $ref->newInstanceArgs($this->args);
        } else {
            $this->table = new $className;
        }

        return $this->table;
    }

    /**
     * creates the table
     */
    protected function buildTable()
    {
        // pass the dependencies in, they can override them later if necessary
        $this->table->setServerProcessService($this->serverProcessService);
        $this->table->setRepository($this->repository);
        $this->table->hydrateObjects = true;
    }

    /**
     * buildMetaData
     *
     * build the columns and other metadata for this class
     */
    abstract protected function buildMetaData();

    /**
     * @param Interfaces\ServerProcessServiceInterface $serverProcessService
     */
    public function setServerProcessService(ServerProcessServiceInterface $serverProcessService)
    {
        $this->serverProcessService = $serverProcessService;
    }

    /**
     * @param \Doctrine\ORM\EntityRepository $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }
}
