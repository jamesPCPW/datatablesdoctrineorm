<?php
namespace Brown298\DataTablesDoctrineORM\Service;

use Brown298\DataTablesModels\Exceptions\ProcessorException;
use Brown298\DataTablesModels\Service\Interfaces\ArrayProcessInterface;
use Brown298\DataTablesModels\Service\Interfaces\ServerProcessServiceInterface;
use Brown298\DataTablesModels\Service\ServerProcessService as BaseServerProcessService;
use Brown298\DataTablesModels\Service\Processor\ArrayProcessor;
use Brown298\DataTablesDoctrineORM\Service\Interfaces\QueryBuilderProcessInterface;
use Brown298\DataTablesDoctrineORM\Service\Interfaces\RepositoryProcessInterface;
use Brown298\DataTablesDoctrineORM\Service\Processor\QueryBuilderProcessor;
use Brown298\DataTablesDoctrineORM\Service\Processor\RepositoryProcessor;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ServerProcessService
 *
 * handles the server side processing
 *
 * @package Brown298\DataTablesBundle\Service
 * @author  John Brown <brown.john@gmail.com>
 */
class ServerProcessService extends BaseServerProcessService implements ServerProcessServiceInterface, QueryBuilderProcessInterface, ArrayProcessInterface, RepositoryProcessInterface
{

    /*----------------------------------------Query Builder-----------------------------------------------------------*/
    /**
     * setQueryBuilder
     *
     * @param QueryBuilder $queryBuilder ]
     * @return mixed|void
     */
    public function setQueryBuilder(QueryBuilder $queryBuilder)
    {
        $this->processor = new QueryBuilderProcessor($queryBuilder, $this->requestParameters, $this->logger);
    }

    /**
     * getQueryBuilder
     *
     * @return null
     */
    public function getQueryBuilder()
    {
        if ($this->processor === null || !($this->processor instanceof QueryBuilderProcessor)) {
            return null;
        }

        return $this->processor->getQueryBuilder();
    }

    /*----------------------------------------Repository Processor----------------------------------------------------*/

    /**
     * setRepository
     *
     * creates a repository based processor
     *
     * @param EntityRepository $repository
     * @return mixed|void
     */
    public function setRepository(EntityRepository $repository)
    {
        $this->processor = new RepositoryProcessor($repository, $this->requestParameters, $this->logger);
    }

    /**
     * getRepository
     *
     * @return Processor\Doctrine\ORM\EntityRepository|null
     */
    public function getRepository()
    {
        if ($this->processor === null || !($this->processor instanceof RepositoryProcessor)) {
            return null;
        }

        return $this->processor->getRepository();
    }

    /**
     * Adds support for magic finders.
     *
     * @param string $method
     * @param array $arguments
     * @return array|object
     * @throws ProcessorException
     */
    public function __call($method, $arguments)
    {
        if ($this->processor === null || !($this->processor instanceof RepositoryProcessor)) {
            Throw new ProcessorException("Generic calls require a Repository Processor, create one by running setRepository");
        }

        return call_user_func_array(array($this->processor,$method), $arguments);
    }

    /**
     * findAll
     *
     * builds a findAll Query
     *
     * @throws \Brown298\DataTablesBundle\Exceptions\ProcessorException
     */
    public function findAll()
    {
        if ($this->processor === null || !($this->processor instanceof RepositoryProcessor)) {
            Throw new ProcessorException("FindAll requires a Repository Processor, create one by running setRepository");
        }

        $this->processor->buildFindAll();
    }

    /**
     * findBy
     *
     * builds a findBy query
     *
     * @param array $criteria
     * @param array $orderBy
     * @param null $limit
     * @param null $offset
     *
     * @throws ProcessorException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        if ($this->processor === null || !($this->processor instanceof RepositoryProcessor)) {
            Throw new ProcessorException("FindBy requires a Repository Processor, create one by running setRepository");
        }

        $this->processor->buildFindBy($criteria, $orderBy, $limit, $offset);
    }
}
